package ultils;

import exception.GenericException;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import java.io.File;

public abstract class CommandUtils {

    private static  Screen screen = new Screen();
    private static String pathImage = new File("images").getAbsolutePath();

    public static void waitAndClick(String urlImage, double time) {
        try {
            screen.wait(new Pattern(pathImage.concat(urlImage)).similar((float) 0.8), time);
            screen.click(pathImage.concat(urlImage));
        } catch (FindFailed e) {
            throw new GenericException("Imagem não encontrada: " + urlImage);
        }
    }

    public static void keypress(String name, double time) {
        screen.wait(time);
        screen.type(name);
    }

    public static void digitText(String text, double time) {
        screen.wait(time);
        screen.type(text);
    }

    public static void ctrlAnd(String ctrl, String letter, double time) {
        screen.wait(time);
        screen.type(letter, ctrl);
    }
}

import flow.ForDevsLoremIpsum;

import java.net.URISyntaxException;

public class Main {
    public static void main(String[] args) throws URISyntaxException {
        ForDevsLoremIpsum forDevs = new ForDevsLoremIpsum();
        forDevs.start();
    }
}

package flow;

import org.sikuli.script.Key;

import static ultils.CommandUtils.*;

public class ForDevsLoremIpsum {

    public void start() {
        this.openChromeAndDigitUrl();
        this.accessUrl();
        this.accessGeradorCpfURL();
        this.textGenerator();
        this.copyTextLoremIpsum();
        this.openNotepadAndPasteText();
        this.saveNotepad();
    }

    private void openChromeAndDigitUrl() {
        waitAndClick("/btn-chrome.PNG", 240.0);
        waitAndClick("/btn-new-tab.PNG", 100.0);
    }

    private void accessUrl() {
        digitText("www.4devs.com.br", 1.0);
        keypress(Key.ENTER, 1.0);
    }

    private void accessGeradorCpfURL() {
        keypress(Key.PAGE_DOWN, 1.0);
        waitAndClick("/btn-gerador-lorem-ipsum.PNG", 1.0);
    }

    private void textGenerator() {
        waitAndClick("/inp-number.PNG", 1.0);
        keypress(Key.BACKSPACE, 1.0);
        digitText("2", 1.0);
        waitAndClick("/btn-gerar-texto.PNG", 1.0);
        keypress(Key.PAGE_DOWN, 1.0);
    }

    private void copyTextLoremIpsum() {
        waitAndClick("/inp-txtarea-loremipsum.PNG", 1.0);
        ctrlAnd(Key.CTRL, "a", 1.0);
        ctrlAnd(Key.CTRL, "c", 1.0);
    }

    private void openNotepadAndPasteText() {
        ctrlAnd(Key.WIN, "r", 1.0);
        digitText("notepad", 1.0);
        keypress(Key.ENTER, 1.0);
        ctrlAnd(Key.CTRL, "v", 1.0);
    }

    private void saveNotepad() {
        waitAndClick("/btn-arquivo.PNG", 1.0);
        waitAndClick("/btn-salvar.PNG", 1.0);
        digitText("treinamento-sikuli", 1.0);
        keypress(Key.ENTER, 1.0);
    }
}
